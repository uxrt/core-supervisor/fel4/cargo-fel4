all:
	cargo build --release
install-local:
	install -m 755 target/release/cargo-fel4 $(TOOLS_INSTALL_ROOT)
clean:
	rm -rf target
